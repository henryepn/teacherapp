//
//  RegisterStudentController.swift
//  TeacherApp
//
//  Created by Henry Aguilar on 1/18/20.
//  Copyright © 2020 Henry. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseStorage
import FirebaseAuth

class RegisterStudentController: UIViewController, UIPickerViewDelegate, UITextFieldDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var dateOfBirthTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    let birthDatePicker = UIDatePicker()
    let type = "Estudiante"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dateOfBirthTextField.delegate = self
        birthDatePicker.datePickerMode = .date
        
        birthDatePicker.addTarget(self, action: #selector(dateValueChange(_:)), for: .valueChanged)

        activityView.hidesWhenStopped = true
        activityView.isHidden = true

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
      view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
      if textField.tag == dateOfBirthTextField.tag {
        textField.inputView = birthDatePicker
      }
    }

    @objc func dateValueChange(_ sender: UIDatePicker) {
        
        let format = DateFormatter()
        format.dateFormat = "dd-MM-YYYY"
        dateOfBirthTextField.text = format.string(from: birthDatePicker.date)
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        activityView.isHidden = false
        activityView.startAnimating()
         
        createUser{ userUID in
            self.saveToFirestore(userUID: userUID){
              // go to next screen
              self.activityView.startAnimating()
                //en el identifier colocar el nombre del enlace a la siguiente pantalla
              //self.performSegue(withIdentifier: "registerSuccessSegue", sender: self)
            }
        }
    }
    
    func saveToFirestore(userUID: String, completionHandler: @escaping ()->()){
      
        let firestore = Firestore.firestore()
       
      //save image to storage and firebase
        firestore.collection("Estudiantes").document(userUID).setData([
            "name" : self.nameTextField.text ?? "",
            "lastname": self.lastNameTextField.text ?? "",
            "birthdate": self.dateOfBirthTextField.text ?? "",
            "phone": self.phoneNumberTextField.text ?? "",
            "email": self.emailTextField.text ?? "",
            "type": self.type
        ])
        { (error) in
            if error != nil {
              self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
            }
            if error == nil {
              completionHandler()
            }
        }
    }
     
    func createUser(completionHandler: @escaping (String)->()){
       
      let auth = Auth.auth()
      auth.createUser(withEmail: emailTextField.text!, password: passwordTextField.text!){ (authInfo, error) in
        if error != nil {
          self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
        }
        if error == nil {
          completionHandler(authInfo!.user.uid)
        }
      }
    }
    
    func showAlert(title: String, message: String){
      let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
       
      let okAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
       
      alertController.addAction(okAlertAction)
       
      present(alertController, animated: true, completion: nil)
    }
    
    

}
