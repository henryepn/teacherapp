//
//  RegisterTeacherController.swift
//  TeacherApp
//
//  Created by Henry Aguilar on 1/25/20.
//  Copyright © 2020 Henry. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseStorage
import FirebaseAuth

class RegisterTeacherController: UIViewController, UINavigationControllerDelegate,
UIImagePickerControllerDelegate,
UITextFieldDelegate,
UIPickerViewDelegate{
    
    @IBOutlet weak var pictureImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var biographyTextField: UITextView!
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    var imagePicker = UIImagePickerController()
    let type = "Profesor"

    override func viewDidLoad() {
        super.viewDidLoad()

        activityView.hidesWhenStopped = true
        activityView.isHidden = true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
      view.endEditing(true)
    }
    
    @IBAction func pictureButtonPressed(_ sender: Any) {
        let actionSheet = UIAlertController(title: "Add Picture", message: nil, preferredStyle: .actionSheet)
         
        let cameraAction = UIAlertAction(title: "Camera", style: .default){(_)
          in
          if UIImagePickerController.isSourceTypeAvailable(.camera){
             
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .camera
            self.imagePicker.allowsEditing = false
             
            self.present(self.imagePicker, animated: true, completion: nil)
          }
        }
         
        let libraryAction = UIAlertAction(title: "Library", style: .default){(_)
          in
          if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
             
            self.imagePicker.delegate = self //mandar a una clase a hacer una tarea
            self.imagePicker.sourceType = .savedPhotosAlbum
            self.imagePicker.allowsEditing = true //permite editar la imagen
             
            self.present(self.imagePicker, animated: true, completion: nil)
          }
        }
         
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(libraryAction)
         
        present(actionSheet, animated: true, completion: nil) //para presentar imagen
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        pictureImageView.image = info[.editedImage] as! UIImage
        picker.dismiss(animated: true, completion: nil)
      }
    
    func uploadImage(userUID: String, completionHandler: @escaping (String)->()){
         
        //let firebase = Firestore.firestore()
        let storage = Storage.storage()
         
        let ref = storage.reference()
        let profileImages = ref.child("users") //creamos carpeta dentro del storage
         
        //let userId = UUID().uuidString //permite generar un ID unico de 32 hexadecimales, algoritmo randomico
         
        //para que no se repitan el nombre de las imagenes, usamos el userId
        let userPicture = profileImages.child("\(userUID).jpg")
         
        //Transformar imagen a lenguaje
        let data = pictureImageView.image?.jpegData(compressionQuality: 0.5)
         
         
        let _ = userPicture.putData(data!, metadata: nil) {(metadata,error) in
          if error != nil {
            self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
          }
          guard let _ = metadata else{
            return
          }
           
          userPicture.downloadURL{(url,error) in
            if error != nil {
              self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
            }
             
            guard let downloadURL = url else{
              return
            }
            completionHandler(downloadURL.absoluteString)
            //self.saveToFirestore(imageURL: downloadURL.absoluteString)
          }
        }
      }
       
      func saveToFirestore(userUID: String, imageURL: String, completionHandler: @escaping ()->()){
        let firestore = Firestore.firestore()
         
        //save image to storage and firebase
        firestore.collection("Profesores").document(userUID).setData(
        [
            "imageURL": "\(imageURL)",
            "name": self.nameTextField.text ?? "",
            "lastname": self.lastNameTextField.text ?? "",
            "phone": self.phoneTextField.text ?? "",
            "email": self.emailTextField.text ?? "",
            "biography": self.biographyTextField.text ?? "",
            "type": self.type
            
        ])
        { (error) in
          if error != nil {
            self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
          }
          if error == nil {
            completionHandler()
          }
        }
         
      }
       
      func createUser(completionHandler: @escaping (String)->()){
         
        let auth = Auth.auth()
        auth.createUser(withEmail: emailTextField.text!, password: passwordTextField.text!){ (authInfo, error) in
          if error != nil {
            self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
          }
          if error == nil {
            completionHandler(authInfo!.user.uid)
          }
        }
      }
       
      func showAlert(title: String, message: String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
         
        let okAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
         
        alertController.addAction(okAlertAction)
         
        present(alertController, animated: true, completion: nil)
        activityView.isHidden = true
        activityView.stopAnimating()
      }
   
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        activityView.isHidden = false
        activityView.startAnimating()
        
        createUser{ userID in
            self.uploadImage(userUID: userID){ imageURL in
                self.saveToFirestore(userUID: userID, imageURL: imageURL){
                    self.activityView.startAnimating()
                    //self.performSegue(withIdentifier: "", sender: self)
                }
            }
        }
    }
    
}
