//
//  ViewController.swift
//  TeacherApp
//
//  Created by Henry Aguilar on 1/14/20.
//  Copyright © 2020 Henry. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

class ViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var userTypeTextField: UILabel!
    
    var typeUser = "Estudiantes"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    //para mantener la sesion iniciada
    /*override func viewDidAppear(_ animated: Bool) {
        if let _ = Auth.auth().currentUser{
            //performSegue(withIdentifier: "loginSuccessSegue", sender: self)//hace que la sesion del usuario se mantenga iniciada, se almacena en el telefono
        }
    }*/
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        view.endEditing(true)
        
    }
    
    private func presentAlerWith(title: String, message: String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAlertAction = UIAlertAction(title: "OK", style: .default){ _ in
            self.emailTextField.text = ""
            self.passwordTextField.text = ""
            self.emailTextField.becomeFirstResponder()
        }
        
        alertController.addAction(okAlertAction)
        //para lanzar un mensaje de aviso
        present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        
        guard let email = emailTextField.text,
            let password = passwordTextField.text else {
            
            return
        }
        //autenticar con firebase
        Auth.auth().signIn(withEmail: email,
                           password: password) { (result, error) in
                            print("RESULT: \(result)")
                            print("ERROR: \(error)")
            if error != nil {
                self.presentAlerWith(title: "Error", message: error?.localizedDescription ?? "ups! An error ocurred")
            }else{
                
                let user = Auth.auth().currentUser
                let userID = user?.uid
                let db = Firestore.firestore()
                print(userID!)
                let docRef = db.collection(self.typeUser).document(userID!)
                
                docRef.getDocument { (document, error) in
                    if let document = document, document.exists {
                        let dataDescription = document.data().map(String.init(describing:)) ?? "nil"
                        print("Document data: \(dataDescription)")
                       if (self.typeUser == "Estudiantes"){
                            self.performSegue(withIdentifier: "loginStudentSegue", sender: self)
                        }else{
                            self.performSegue(withIdentifier: "loginTeacherSegue", sender: self)
                        }
                    }else {
                        print("Document does not exist")
                    }
                }
                
            }
        }
        
    }
    
    
    @IBAction func typeUserSwitch(_ sender: UISwitch) {
        if(sender.isOn == true){
            self.userTypeTextField.text = "I am a teacher"
            self.typeUser = "Profesores"
        }else{
            self.userTypeTextField.text = "I am a student"
            self.typeUser = "Estudiantes"
        }
    }
    
    
    
}

